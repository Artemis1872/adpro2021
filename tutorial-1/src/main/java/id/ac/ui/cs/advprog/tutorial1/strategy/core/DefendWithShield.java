package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {

    String type;
    String defend;

    public DefendWithShield() {
        this.type = "shield";
        this.defend = "mengtameng";
    }

    @Override
    public String defend() {
        return this.defend;
    }

    @Override
    public String getType() {
        return this.type;
    }
}
