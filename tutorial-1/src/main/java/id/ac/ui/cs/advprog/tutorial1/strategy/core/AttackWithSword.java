package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {

    String type;
    String attack;

    public AttackWithSword() {
        this.attack = "mengkobel-kobel";
        this.type = "sword";
    }

    @Override
    public String attack() {
        return this.attack;
    }

    @Override
    public String getType() {
        return this.type;
    }
}
