package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {

    String type;
    String attack;

    public AttackWithMagic() {
        this.attack = "mengsantet";
        this.type = "magic";
    }

    @Override
    public String attack() {
        return this.attack;
    }

    @Override
    public String getType() {
        return this.type;
    }
}
