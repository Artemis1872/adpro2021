package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    String type;
    String attack;

    public AttackWithGun() {
        this.attack = "mengpestol";
        this.type = "gun";
    }

    @Override
    public String attack() {
        return this.attack;
    }

    @Override
    public String getType() {
        return this.type;
    }
}
