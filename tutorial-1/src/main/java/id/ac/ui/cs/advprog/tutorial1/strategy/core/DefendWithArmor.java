package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {

    String type;
    String defend;

    public DefendWithArmor() {
        this.type = "armor";
        this.defend ="mengarmor";
    }

    @Override
    public String defend() {
        return this.defend;
    }

    @Override
    public String getType() {
        return this.type;
    }
}
