package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {

    protected AttackBehavior attackBehavior;
    protected DefenseBehavior defenseBehavior;

    public KnightAdventurer() {
        this.attackBehavior = new AttackWithSword();
        this.defenseBehavior = new DefendWithArmor();
    }

    public void setAttackBehavior(AttackBehavior attackBehavior) {
        this.attackBehavior = attackBehavior;
    }

    public AttackBehavior getAttackBehavior() {
        return attackBehavior;
    }

    public void setDefenseBehavior(DefenseBehavior defenseBehavior) {
        this.defenseBehavior = defenseBehavior;
    }

    public DefenseBehavior getDefenseBehavior() {
        return defenseBehavior;
    }

    public String attack () {
        return this.attackBehavior.attack();
    }

    public String defend() {
        return this.defenseBehavior.defend();
    }

    @Override
    public String getAlias() {
        return "Knight Warrior - Satria";
    }
}
