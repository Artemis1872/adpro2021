package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {

    String type;
    String defend;

    public DefendWithBarrier() {
        this.type = "barrier";
        this.defend = "menghadang";
    }

    @Override
    public String defend() {
        return this.defend;
    }

    @Override
    public String getType() {
        return this.type;
    }
}
