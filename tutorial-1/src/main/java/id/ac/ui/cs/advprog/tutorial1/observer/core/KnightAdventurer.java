package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.guild = guild;
        this.name = "Knight";
        guild.add(this);
    }

    @Override
    public void update() {
        Quest quest = this.guild.getQuest();
        this.getQuests().add(quest); // Accept every quest
    }
}
