package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;

public class MenuFactoryImpl implements MenuFactory {

    @Override
    public Menu createMenu(String name, String type) {
        switch (type) {
            case "LiyuanSoba":
                return new LiyuanSoba(name);
            case "InuzumaRamen":
                return new InuzumaRamen(name);
            case "MondoUdon":
                return new MondoUdon(name);
            case "SnevnezhaShirataki":
                return new SnevnezhaShirataki(name);
            default:
                System.out.println(
                    String.format("[ERROR] --- Menu Type: %s not in menu", name, type));
                return null;
        }
    }
}
