package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactoryImpl;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {
    private MenuRepository repo;
    private MenuFactory factory;

    public MenuServiceImpl(MenuRepository repo){
        this.repo = repo;
        factory = new MenuFactoryImpl();
        initRepo();
    }

    public MenuServiceImpl(){
        this(new MenuRepository());
    }

    public Menu createMenu(String name, String type){
        Menu res = factory.createMenu(name, type);
        if (res == null) return null; // nothing breaks amen
        return repo.add(res);
    }

    public List<Menu> getMenus(){
        return repo.getMenus();
    }

    private void initRepo(){
        createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
        createMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen");
        createMenu("Good Hunter Cheese Chicken Udon", "MondoUdon");
        createMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki");
    }
}