package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Test;

class SweetTest {

    @Test
    public void testSweetCorrectDescription() {
        assertEquals("Adding a dash of Sweet Soy Sauce...", new Sweet().getDescription());
    }

}