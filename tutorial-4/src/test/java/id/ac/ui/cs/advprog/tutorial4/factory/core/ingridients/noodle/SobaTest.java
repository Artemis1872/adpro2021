package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SobaTest {

    @Test
    public void testSobaDescriptionIsCorrect() {
        assertEquals("Adding Liyuan Soba Noodles...", new Soba().getDescription());
    }

}