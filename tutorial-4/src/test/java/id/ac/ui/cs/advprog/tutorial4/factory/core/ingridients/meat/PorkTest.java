package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PorkTest {

    @Test
    public void testPorkDescriptionIsCorrect() {
        assertEquals("Adding Tian Xu Pork Meat...", new Pork().getDescription());
    }


}