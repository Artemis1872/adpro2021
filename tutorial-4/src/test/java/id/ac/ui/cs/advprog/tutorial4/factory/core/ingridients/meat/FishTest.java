package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FishTest {

    @Test
    public void testFishDescriptionIsCorrect() {
        assertEquals("Adding Zhangyun Salmon Fish Meat...", new Fish().getDescription());
    }

}