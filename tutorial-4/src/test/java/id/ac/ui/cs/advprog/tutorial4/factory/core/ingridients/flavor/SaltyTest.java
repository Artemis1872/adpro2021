package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Test;

class SaltyTest {

    @Test
    public void testSaltyCorrectDescription() {
        assertEquals("Adding a pinch of salt...", new Salty().getDescription());
    }

}