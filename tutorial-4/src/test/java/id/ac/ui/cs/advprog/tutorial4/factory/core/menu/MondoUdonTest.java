package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.Test;

class MondoUdonTest {

    @Test
    public void testRamenIngredients() {
        MondoUdon mondoUdon = new MondoUdon("Dummy Ramen");
        assertEquals("Dummy Ramen", mondoUdon.getName());
        assertEquals(mondoUdon.getNoodle().getClass().getName(), Udon.class.getName());
        assertEquals(mondoUdon.getMeat().getClass().getName(), Chicken.class.getName());
        assertEquals(mondoUdon.getTopping().getClass().getName(), Cheese.class.getName());
        assertEquals(mondoUdon.getFlavor().getClass().getName(), Salty.class.getName());
    }

}