package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ShiratakiTest {

    @Test
    public void testShiratakiDescriptionIsCorrect() {
        assertEquals("Adding Snevnezha Shirataki Noodles...", new Shirataki().getDescription());
    }

}