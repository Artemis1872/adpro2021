package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.Test;

class InuzumaRamenTest {

    @Test
    public void testRamenIngredients() {
        InuzumaRamen inuzumaRamen = new InuzumaRamen("Dummy Ramen");
        assertEquals("Dummy Ramen", inuzumaRamen.getName());
        assertEquals(inuzumaRamen.getNoodle().getClass().getName(), Ramen.class.getName());
        assertEquals(inuzumaRamen.getMeat().getClass().getName(), Pork.class.getName());
        assertEquals(inuzumaRamen.getTopping().getClass().getName(), BoiledEgg.class.getName());
        assertEquals(inuzumaRamen.getFlavor().getClass().getName(), Spicy.class.getName());
    }

}