package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {

    private Class<?> orderServiceClass;

    @InjectMocks
    private OrderServiceImpl orderService;

    @BeforeEach
    public void setUp() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void testOrderServiceGetDrinkMethod() throws Exception {
        Method getDrink = orderServiceClass.getDeclaredMethod("getDrink");
        assertEquals(
            "id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink",
            getDrink.getGenericReturnType().getTypeName()
        );
        assertEquals(0, getDrink.getParameterCount());
        assertTrue(Modifier.isPublic(getDrink.getModifiers()));
    }

    @Test
    public void testOrderServiceGetFoodMethod() throws Exception {
        Method getDrink = orderServiceClass.getDeclaredMethod("getFood");
        assertEquals(
            "id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood",
            getDrink.getGenericReturnType().getTypeName()
        );
        assertEquals(0, getDrink.getParameterCount());
        assertTrue(Modifier.isPublic(getDrink.getModifiers()));
    }

    @Test
    public void testOrderServiceMethodsReturnsAnInstance() {
        assertNotNull(orderService.getFood());
        assertNotNull(orderService.getDrink());
    }

    @Test
    public void testOrderServiceOrderAFood() {
        orderService.orderAFood("Tonkotsu Ramen");
        assertEquals(orderService.getFood().getFood(), "Tonkotsu Ramen");
    }

    @Test
    public void testOrderServiceOrderADrink() {
        orderService.orderADrink("Soda Gembira");
        assertEquals(orderService.getDrink().getDrink(), "Soda Gembira");
    }

}