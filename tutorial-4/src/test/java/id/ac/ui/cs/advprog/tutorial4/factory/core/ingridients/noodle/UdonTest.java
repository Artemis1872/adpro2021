package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UdonTest {

    @Test
    public void testUdonDescriptionIsCorrect() {
        assertEquals("Adding Mondo Udon Noodles...", new Udon().getDescription());
    }
}