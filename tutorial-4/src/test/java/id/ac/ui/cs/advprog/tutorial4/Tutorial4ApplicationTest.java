package id.ac.ui.cs.advprog.tutorial4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Tutorial4ApplicationTest {

    @Test
    // https://stackoverflow.com/a/49345638
    public void main() {
        Tutorial4Application.main(new String[] {});
    }

}