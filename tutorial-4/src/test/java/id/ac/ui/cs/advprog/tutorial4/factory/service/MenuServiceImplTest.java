package id.ac.ui.cs.advprog.tutorial4.factory.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactoryImpl;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;

class MenuServiceImplTest {

    private Class<?> menuServiceClass;

    @InjectMocks
    private MenuService menuService;
    @InjectMocks
    private MenuRepository repo;
    @InjectMocks
    private MenuFactory factory;

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName(
            "id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }

    @Test
    public void testMenuServiceHasCreateMenuMethod() throws Exception {
        Class<?>[] args = {String.class, String.class};
        Method createMenu = menuServiceClass.getDeclaredMethod("createMenu", args);
        assertEquals(2, createMenu.getParameterCount());
        int methodModifiers = createMenu.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu", createMenu.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMenuServiceGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        ParameterizedType pt = (ParameterizedType) getMenus.getGenericReturnType();
        assertEquals(List.class, pt.getRawType());
        assertTrue(Arrays.asList(pt.getActualTypeArguments()).contains(Menu.class));
    }

    @Test
    public void testRepoInitializarion() {
        MenuService menuService1 = new MenuServiceImpl();
        assertEquals(4, menuService1.getMenus().size());
    }
}