package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ChickenTest {

    @Test
    public void testChickenDescriptionIsCorrect() {
        assertEquals("Adding Wintervale Chicken Meat...", new Chicken().getDescription());
    }
}