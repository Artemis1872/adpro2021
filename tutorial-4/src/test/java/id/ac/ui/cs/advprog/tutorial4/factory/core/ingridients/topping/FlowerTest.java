package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FlowerTest {

    @Test
    public void testFlowerDescriptionIsCorrect() {
        assertEquals("Adding Xinqin Flower Topping...", new Flower().getDescription());
    }

}