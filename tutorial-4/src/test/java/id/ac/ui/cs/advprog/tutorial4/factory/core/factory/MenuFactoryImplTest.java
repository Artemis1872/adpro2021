package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

class MenuFactoryImplTest {

    @Test
    public void testCreateMenuCreatesLiyuanObject() {
        Menu menu = new MenuFactoryImpl().createMenu("Dummy", "LiyuanSoba");
        assertEquals(menu.getName(), "Dummy");
        assertEquals(menu.getClass().getName(), LiyuanSoba.class.getName());
    }

    @Test
    public void testCreateMenuCreatesInuzumaObject() {
        Menu menu = new MenuFactoryImpl().createMenu("Dummy", "InuzumaRamen");
        assertEquals(menu.getName(), "Dummy");
        assertEquals(menu.getClass().getName(), InuzumaRamen.class.getName());
    }

    @Test
    public void testCreateMenuCreatesMondoObject() {
        Menu menu = new MenuFactoryImpl().createMenu("Dummy", "MondoUdon");
        assertEquals(menu.getName(), "Dummy");
        assertEquals(menu.getClass().getName(), MondoUdon.class.getName());
    }

    @Test
    public void testCreateMenuCreatesSnevnezhaObject() {
        Menu menu = new MenuFactoryImpl().createMenu("Dummy", "SnevnezhaShirataki");
        assertEquals(menu.getName(), "Dummy");
        assertEquals(menu.getClass().getName(), SnevnezhaShirataki.class.getName());
    }

    @Test
    public void testCreateMenuCreatesMenuNotFound() {
        Menu menu = new MenuFactoryImpl().createMenu("Dummy", "Nonexistend");
        assertNull(menu);

    }

}