package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import org.assertj.core.util.VisibleForTesting;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

class OrderFoodTest {

    private Class<?> orderFoodClass;
    private OrderFood orderFood;
    private ClassLoader loader;

    @BeforeEach
    public void setUp() throws Exception {
        orderFoodClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood");
        orderFood = OrderFood.getInstance();
        loader = this.getClass().getClassLoader();
    }

    @Test
    public void testOrderFoodisConreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(orderFoodClass.getModifiers()));
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(orderFoodClass.getDeclaredConstructors());

        boolean check = constructors.stream()
            .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertEquals(false, check);
    }

    @Test
    public void testGetFoodMethod() throws Exception {
        Method getFood = orderFoodClass.getDeclaredMethod("getFood");
        assertEquals("java.lang.String", getFood.getGenericReturnType().getTypeName());
        assertEquals(0, getFood.getParameterCount());
        assertTrue(Modifier.isPublic(getFood.getModifiers()));
    }

    @Test
    public void testTwoInstanceIsPointingOnSameReference() {
        OrderFood OrderFood1 = OrderFood.getInstance();
        OrderFood OrderFood2 = OrderFood.getInstance();
        assertSame(OrderFood1, OrderFood2);
    }

    @Test
    public void testTwoInstanceIsTheSameObject() {
        OrderFood OrderFood1 = OrderFood.getInstance();
        OrderFood OrderFood2 = OrderFood.getInstance();
        assertEquals(OrderFood1, OrderFood2);
    }

    @Test
    public void testDefaultFoodIsNotNull() {
        OrderFood OrderFood1 = OrderFood.getInstance();
        assertNotNull(OrderFood1.getFood());
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        OrderDrink orderDrink = OrderDrink.getInstance();
        assertNotNull(orderDrink);
    }

    @Test
    public void testOrderFood(){
        orderFood.setFood("Tonkotsu Ramen");
        assertEquals("Tonkotsu Ramen", orderFood.getFood());
        assertNotNull(orderFood.toString());
    }

//    @VisibleForTesting
//public void testConstructorException() throws Exception {
//    Class<?> orderFoodClass = loader.loadClass("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood");
//    Thread.currentThread().interrupt();
//    assertThrows(orderFoodClass.)
//}
}