package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import org.junit.jupiter.api.Test;

class BoiledEggTest {

    @Test
    public void testBoiledEggDescriptionIsCorrect() {
        assertEquals("Adding Guahuan Boiled Egg Topping", new BoiledEgg().getDescription());
    }

}