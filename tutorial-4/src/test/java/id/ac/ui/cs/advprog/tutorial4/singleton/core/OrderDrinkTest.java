package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

class OrderDrinkTest {

    private Class<?> orderDrinkClass;
    private OrderDrink orderDrink;

    @BeforeEach
    public void setUp() throws Exception {
        orderDrinkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink");
        orderDrink = OrderDrink.getInstance();
    }

    @Test
    public void testOrderDrinkisConreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(orderDrinkClass.getModifiers()));
    }

    @Test
    public void testOrderDrinkNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(orderDrinkClass.getDeclaredConstructors());

        boolean check = constructors.stream()
            .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertEquals(false, check);
    }

    @Test
    public void testGetDrinkMethod() throws Exception {
        Method getDrink = orderDrinkClass.getDeclaredMethod("getDrink");
        assertEquals("java.lang.String", getDrink.getGenericReturnType().getTypeName());
        assertEquals(0, getDrink.getParameterCount());
        assertTrue(Modifier.isPublic(getDrink.getModifiers()));
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        OrderDrink orderDrink = OrderDrink.getInstance();

        assertNotNull(orderDrink);
    }

    @Test
    public void testTwoInstanceIsPointingOnSameReference() {
        OrderDrink orderDrink1 = OrderDrink.getInstance();
        OrderDrink orderDrink2 = OrderDrink.getInstance();
        assertSame(orderDrink1, orderDrink2);
    }

    @Test
    public void testTwoInstanceIsTheSameObject() {
        OrderDrink orderDrink1 = OrderDrink.getInstance();
        OrderDrink orderDrink2 = OrderDrink.getInstance();
        assertEquals(orderDrink1, orderDrink2);
    }

    @Test
    public void testDefaultDrinkIsNotNull() {
        OrderDrink orderDrink1 = OrderDrink.getInstance();
        assertNotNull(orderDrink1.getDrink());
    }

    @Test
    public void testOrderDrink(){
        orderDrink.setDrink("Bir Bintang");
        assertEquals("Bir Bintang", orderDrink.getDrink());
        assertNotNull(orderDrink.toString());
    }
}