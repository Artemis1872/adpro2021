package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CheeseTest {

    @Test
    public void testCheeseDescriptionIsCorrect() {
        assertEquals("Adding Shredded Cheese Topping...", new Cheese().getDescription());
    }


}