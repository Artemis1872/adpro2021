package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.Test;

class RamenTest {

    @Test
    public void testRamenDescriptionIsCorrect() {
        assertEquals("Adding Inuzuma Ramen Noodles...", new Ramen().getDescription());
    }

}