package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.Test;

class LiyuanSobaTest {

    @Test
    public void testRamenIngredients() {
        LiyuanSoba liyuanSoba = new LiyuanSoba("Dummy Ramen");
        assertEquals("Dummy Ramen", liyuanSoba.getName());
        assertEquals(liyuanSoba.getNoodle().getClass().getName(), Soba.class.getName());
        assertEquals(liyuanSoba.getMeat().getClass().getName(), Beef.class.getName());
        assertEquals(liyuanSoba.getTopping().getClass().getName(), Mushroom.class.getName());
        assertEquals(liyuanSoba.getFlavor().getClass().getName(), Sweet.class.getName());
    }

}