package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MushroomTest {

    @Test
    public void testMushroomDescriptionIsCorrect() {
        assertEquals("Adding Shiitake Mushroom Topping...", new Mushroom().getDescription());
    }


}