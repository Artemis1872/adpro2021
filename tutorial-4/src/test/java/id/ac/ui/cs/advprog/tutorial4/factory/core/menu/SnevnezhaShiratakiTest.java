package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.Test;

class SnevnezhaShiratakiTest {

    @Test
    public void testRamenIngredients() {
        SnevnezhaShirataki snevnezhaShirataki = new SnevnezhaShirataki("Dummy Ramen");
        assertEquals("Dummy Ramen", snevnezhaShirataki.getName());
        assertEquals(snevnezhaShirataki.getNoodle().getClass().getName(), Shirataki.class.getName());
        assertEquals(snevnezhaShirataki.getMeat().getClass().getName(), Fish.class.getName());
        assertEquals(snevnezhaShirataki.getTopping().getClass().getName(), Flower.class.getName());
        assertEquals(snevnezhaShirataki.getFlavor().getClass().getName(), Umami.class.getName());
    }

}