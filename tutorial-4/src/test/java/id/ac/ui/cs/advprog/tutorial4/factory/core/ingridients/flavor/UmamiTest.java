package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Test;

class UmamiTest {

    @Test
    public void testUmamiCorrectDescription() {
        assertEquals("Adding WanPlus Specialty MSG flavoring...", new Umami().getDescription());
    }

}