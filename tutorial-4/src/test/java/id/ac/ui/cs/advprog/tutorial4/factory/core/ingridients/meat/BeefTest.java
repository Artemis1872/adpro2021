package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BeefTest {

    @Test
    public void testBeefDescriptionIsCorrect() {
        assertEquals("Adding Maro Beef Meat...", new Beef().getDescription());
    }

}