package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.Test;

class MenuRepositoryTest {

    @Test
    public void testMenuRepositoryHaveEmptArrayAsDefault() {
        MenuRepository menuRepository = new MenuRepository();
        assertNotNull(menuRepository.getMenus());
        assertEquals(0, menuRepository.getMenus().size());
    }

    @Test
    public void testMenuRepositoryAddMethod() {
        MenuRepository menuRepository = new MenuRepository();
        menuRepository.add(new InuzumaRamen("Dummy1"));
        menuRepository.add(new SnevnezhaShirataki("Dummy2"));
        assertNotNull(menuRepository.getMenus());
        assertEquals(2, menuRepository.getMenus().size());
    }

}