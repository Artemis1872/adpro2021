# Eager vs Lazy Singleton
This repo implements both Eager  (`core/OrderFood.java`) and Lazy
(`core/OrderDrink.java`) instantiation. But what's the difference
between those two?

### Eager vs Lazy Instantiation
Lazy instantiation means  an  instance will only be created when
the instance getter method is  called. While Eager instantiation
will create an instance when the class is loaded.

### Why Lazy?
Lazy Instantiation will prevent unnecessary object creation.
This is practically  useful if we have a very *heavy* object. To
avoid performance and memory issue,  we want to only instantiate
the object only when necessary.

### Why avoid Lazy?
When you're doing multithreading, lazy instantiation will
introduce performance issue due to race conditions. We can avoid
race condition by using `synchronized` keyword, but it will also
took a lot more resource than not using `synchronized` keyword.

### Why Eager?
Eager  instantiation  will  create an  instance at class loading.
This  means  the object will already available when the instance
getter method is called.  Eager  will be particularly useful for
multithreading since we don't have to waste resources for a 
`synchronized` keyword.

### Why avoid Eager?
Eager instantiation should be avoided when the object creation 
is heavy enough and start to affect performance. Or when resources
are limited.