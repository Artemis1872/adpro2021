package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean largeSpellWasInvoked;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.largeSpellWasInvoked = false;
    }

    @Override
    public String normalAttack() {
        this.largeSpellWasInvoked = false;
        return this.spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!this.largeSpellWasInvoked) {
            this.largeSpellWasInvoked = true;
            return this.spellbook.largeSpell();
        }
        this.largeSpellWasInvoked = false;
        return "Magic power not enough for large spell";
    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }

}
