package id.ac.ui.cs.advprog.tutorial3.facade.core.helper;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.BrutusTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the facade class
 */
public class Transformer {

    protected List<Transformation> transformations;

    public Transformer() {
        this.transformations = new ArrayList<>();
        transformations.add(new CelestialTransformation());
        transformations.add(new AbyssalTransformation());
        transformations.add(new BrutusTransformation());
    }

    public String encode(String text) {
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        for (Transformation transformation : transformations) {
            spell = transformation.encode(spell);
        }
        return CodexTranslator.translate(spell, RunicCodex.getInstance()).getText();
    }

    public String decode(String text) {
        Spell spell = new Spell(text, RunicCodex.getInstance());
        for (int i=transformations.size(); 0 < i--;) {
            spell = transformations.get(i).decode(spell);
        }
        return CodexTranslator.translate(spell, AlphaCodex.getInstance()).getText();
    }
}
