package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

/**
 * Kelas ini melakukan cipher Caesar
 */
public class BrutusTransformation implements Transformation {
    private int key;

    public BrutusTransformation(int key){
        this.key = key;
    }

    public BrutusTransformation(){
        this(13);
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int codexSize = codex.getCharSize();
        int offset = encode ? key : codexSize-key;
        char[] res = new char[text.length()];
        for(int i = 0; i < res.length; i++){
            char oldChar = text.charAt(i);
            int charIdx = codex.getIndex(oldChar);
            int newCharIdx = (charIdx + offset) % codexSize;
            res[i] = codex.getChar(newCharIdx);
        }

        return new Spell(new String(res), codex);
    }
}
