package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean isAim;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.isAim = false;
    }

    @Override
    public String normalAttack() {
        return this.bow.shootArrow(isAim);
    }

    @Override
    public String chargedAttack() {
        this.isAim = !this.isAim;
        if (this.isAim) return "Entering aim shot mode";
        return "Leaving aim shot mode";
    }

    @Override
    public String getName() {
        return this.bow.getName();
    }

    @Override
    public String getHolderName() {
        return this.bow.getHolderName();
    }
}
