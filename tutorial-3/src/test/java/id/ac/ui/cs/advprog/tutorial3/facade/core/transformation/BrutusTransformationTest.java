package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import static org.junit.jupiter.api.Assertions.*;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BrutusTransformationTest {
    private Class<?> brutusClass;

    @BeforeEach
    public void setup() throws Exception {
        brutusClass = Class.forName(
            "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.BrutusTransformation");
    }

    @Test
    public void testBrutusHasEncodeMethod() throws Exception {
        Method translate = brutusClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testBrutusEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "fnsv4nMn0qMVM9r06M61MnMoynpx5zv6uM61Ms14trM174M5914q";

        Spell result = new BrutusTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testBrutusHasDecodeMethod() throws Exception {
        Method translate = brutusClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testBrutusDecodesCorrectly() throws Exception {
        String text = "fnsv4nMn0qMVM9r06M61MnMoynpx5zv6uM61Ms14trM174M5914q";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new BrutusTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}