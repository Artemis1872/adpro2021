package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.List;

public class ChainSpell implements Spell {
    List<Spell> chainSpell;

    public ChainSpell(List<Spell> chainSpell) {
        this.chainSpell = chainSpell;
    }

    @Override
    public void cast() {
        for (Spell spell : chainSpell) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        // TODO
        for (int i = chainSpell.size(); i-- > 0; ) {
            Spell spell = chainSpell.get(i);
            if (!spell.spellName().contains(":Seal")) spell.cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
